<?php
/**
 * @file
 * Theme function for mobilegallery and its submodules.
 *
 * These theme functions are actually just helper functions
 * for other preprocess functions. For example theme_mobilegallery_prepro_views_rows
 * gets called from template_preprocess_mobilegallery_simple_rows and
 * mobilegallery_semanticviews_preprocess_semanticviews_view_unformatted
 *
 * The function mobilegallery_render is also used so that themers can do
 * very specific overrides of these theme functions.
 */

/**
 * Add markup to each row in a mobilegallery result
 *
 * This function is called from template_preprocess_mobilegallery_simple_rows and
 * mobilegallery_semanticviews_preprocess_semanticviews_view_unformatted
 *
 * This function adds the navigation div and the caption along with
 * html classes and an html id to each row.
 *
 * @param $view
 *   A Views object.
 * @param $vars
 *   The variables array from a preprocess function
 * @return
 *   An altered variables array
 */
function theme_mobilegallery_prepro_views_rows($view, $vars) {

    //The number from which these rows will start counting up.
    $starting_index = $view->pager['current_page'] * $view->pager['items_per_page'];

    //Add classes and ids to the row attributes
    foreach ($vars['rows'] as $key => $value) {
      $vars['row_attributes'][$key]['id'] = 'mg-' . ($starting_index + $key);
      $vars['row_attributes'][$key]['class'] .= ' mobilegallery-rows';
    }

    //Get the caption field
    $caption_field = $view->style_plugin->options['caption_field'];

    //Tack on the navigation and caption to the end of each row
    foreach ($vars['rows'] as $key => $value) {

      $vars['rows'][$key] .= mobilegallery_render('mobilegallery_navigation', $view, $starting_index, $key);

      if ($caption_field) {
        $vars['rows'][$key] .= mobilegallery_render('mobilegallery_caption', $view, $caption_field, $key);
      }
    }

  return $vars;
}


/**
 * Alters the variables sent to the views_view template
 *
 * This function is called from mobilegallery_preprocess_views_view
 *
 * Depending on user configuration, this function may do all or some of
 * the following: alter the pager, add thumbnails, add CSS and JS to the page,
 * kill the page request and just send JSON.
 *
 *
 * @param $view
 *   A Views object.
 * @param $vars
 *   The variables array from a preprocess function
 * @return
 *   An altered variables array
 */
function theme_mobilegallery_prepro_views_views_vars($view, $vars) {

  $showmore = '';
  //If the view is using a pager, get the show more link to replace it
  if ($view->style_options['ajax'] == '1' && $view->pager['use_pager'] != '0') {
    $showmore = mobilegallery_render('mobilegallery_showmore_link', $view);
  }

  //Get the thumbnails
  $thumbnails = mobilegallery_render('mobilegallery_thumbnails', $view);

  //If this is a json page, kill the page now
  if ($_GET['mobilegallery_json'] == '1') {

    $json = array(
      'showmore_link' => $showmore,
      'thumbnails'    => $thumbnails,
      'rows'       => $vars['rows'],
    );

    //output the JSON and kill the page
    drupal_json($json);
    die();
  }
  else {

    //Add thumbnails to the rows variable
    $vars['rows'] = mobilegallery_render('mobilegallery_gallery', $view, $thumbnails, $vars['rows']);

    //Add the show more link to the pager
    if (!empty($showmore)) {
      $vars['pager'] .= '<div class="mobilegallery-showmore-wrapper">' . $showmore . '</div>';
    }

    //If not returning JSON, Add CSS and JS

    mobilegallery_render('mobilegallery_add_js_and_css', $view);
  }

  return $vars;
}
