<?php
/**
 * @file
 * Provides Views plugin for image galleries optimized for mobile devices
 */

/**
 * Implementation of hook_theme().
 */
function mobilegallery_theme() {

  return array(

    'mobilegallery_gallery' => array(
      'arguments' => array(
        'view' => NULL,
        'thumbnails' => NULL,
        'rows' => NULL,
      ),
      'template'  => 'mobilegallery-gallery',
      'file' => 'mobilegallery.theme.inc',
    ),

    'mobilegallery_showmore_link' => array(
      'arguments' => array(
        'view' => NULL,
      ),
      'file' => 'mobilegallery.theme.inc',
    ),

    'mobilegallery_thumbnails' => array(
      'arguments' => array(
        'view' => NULL,
      ),
      'file' => 'mobilegallery.theme.inc',
    ),

    'mobilegallery_caption' => array(
      'arguments' => array(
        'view' => NULL,
        'caption_field' => '',
        'key' => 0,
      ),
      'file' => 'mobilegallery.theme.inc',
    ),

    'mobilegallery_navigation' => array(
      'arguments' => array(
        'view' => NULL,
        'starting_index' => 0,
        'key' => 0,
      ),
      'file' => 'mobilegallery.theme.inc',
    ),

    'mobilegallery_close' => array(
      'arguments' => array(
        'view' => NULL,
      ),
      'file' => 'mobilegallery.theme.inc',
    ),

    'mobilegallery_prepro_views_rows' => array(
      'arguments' => array(
        'view' => NULL,
        'vars' => NULL,
      ),
      'file' => 'mobilegallery.prepro.theme.inc',
    ),

    'mobilegallery_prepro_views_views_vars' => array(
      'arguments' => array(
        'view' => NULL,
        'vars' => NULL,
      ),
      'file' => 'mobilegallery.theme.inc',
    ),

    'mobilegallery_add_js_and_css' => array(
      'arguments' => array(
        'view' => NULL,
      ),
      'file' => 'mobilegallery.prepro.theme.inc',
    ),
  );
}

/**
 * Add variables to the views_view template
 *
 */
function mobilegallery_preprocess_views_view($vars, $hook) {

  $view = $vars['view'];

  //Is this a mobilegallery View?
  if (strpos($view->style_plugin->definition['handler'], '_mobilegallery')) {
    $vars = mobilegallery_render('mobilegallery_prepro_views_views_vars', $view, $vars);
  }
}

/**
 * This function takes a single theme hook and converts it to an array of hooks
 *
 * theme() can take a single string or an array of suggestion hooks.
 *
 * For example, if the hook being passed is 'mobilegallery_close' and
 * the submodule being used for the style plugin is 'mobilegallery_simple'
 * and the view name is 'imagecenter' and the variant is 'panel_pane_1',
 * the returned array would be:
 * array(
 *   'mobilegallery_close__mobilegallery_simple__imagecenter__panel_pane_1',
 *   'mobilegallery_close__mobilegallery_simple__panel_pane_1',
 *   'mobilegallery_close__mobilegallery_simple__',
 *   'mobilegallery_close__mobilegallery_simple__imagecenter__panel_pane',
 *   'mobilegallery_close__mobilegallery_simple__panel_pane',
 *   'mobilegallery_close__mobilegallery_simple__imagecenter',
 *   'mobilegallery_close__mobilegallery_simple',
 *   'mobilegallery_close__imagecenter__panel_pane_1',
 *   'mobilegallery_close__panel_pane_1',
 *   'mobilegallery_close__',
 *   'mobilegallery_close__imagecenter__panel_pane',
 *   'mobilegallery_close__panel_pane',
 *   'mobilegallery_close__imagecenter',
 *   'mobilegallery_close',
 * );
 *
 * @param $hook
 *   The basic name of a theme function.
 * @param $view
 *   A Views object.
 * @return
 *   An array of potential hook names to be passed on to theme()
 */
function mobilegallery_theme_functions($hook, $view) {

  return array_merge(
    views_theme_functions($hook . '__' . $view->plugin_name, $view, $view->display[$view->current_display]),
    views_theme_functions($hook, $view, $view->display[$view->current_display])
  );
}

/**
 * Passes variables on to theme()
 *
 * By working with mobilegallery_theme_functions, mobilegallery_render
 * takes single hook name and then sends to theme() and array of potential
 * hook names.
 *
 * This is used so that submodules can easily override given them functions.
 * For example mobilegallery_simple needs its own version of theme_mobilegallery_thumbnails
 * so it declares theme_mobilegallery_thumbnails__mobilegallery_simple.
 * Then when theme_mobilegallery_prepro_views_views_vars runs
 * mobilegallery_render('mobilegallery_thumbnails', $view) the more specific option,
 * theme_mobilegallery_thumbnails__mobilegallery_simple, is used.
 *
 * @return
 *   The results of theme()
 */
function mobilegallery_render() {

  $args = func_get_args();
  $hook = $args[0];
  $view = $args[1];
  $args[0] = mobilegallery_theme_functions($hook, $view);

  return call_user_func_array('theme', $args);
}
