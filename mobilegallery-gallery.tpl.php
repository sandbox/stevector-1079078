<?php
/**
 * @file
 * The template for mobilegallery_gallery
 */
?>
<?php if (!empty($thumbnails)) { ?>
<div class="mobilegallery-thumbnails-wrapper clearfix">
  <?php print $thumbnails; ?>
</div>
<?php } ?>

<?php if (!empty($close_link)) { ?>
<div class="mobilegallery-close-wrapper">
  <?php  print $close_link;  ?>
</div>
<?php } ?>

<?php if (!empty($rows)) { ?>
<div class="mobilegallery-rows-wrapper">
  <?php  print $rows;  ?>
</div>
<?php } ?>
