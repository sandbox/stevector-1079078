<?php

/**
 * @file
 * Contains the mobilegallery_semantic style plugin.
 */

/**
 * Style plugin to provide a mobile image gallery
 *
 * @ingroup views_style_plugins
 */
class mobilegallery_semanticviews_plugin_style_mobilegallery_semanticviews extends semanticviews_plugin_style_default  {

  /**
   * Render the given style.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // Pre-build all of our option lists for the dials and switches that follow.
    $fields = array('' => t('<none>'));
    foreach ($this->display->handler->get_handlers('field') as $field => $handler) {
      if ($label = $handler->label()) {
        $fields[$field] = $label;
      }
      else {
        $fields[$field] = $handler->ui_name();
      }
    }

    //Select which field to use for thumbnails
    $form['thumbnail_field'] = array(
      '#type' => 'select',
      '#title' => t('thumbnail field'),
      '#options' => $fields,
      '#default_value' => $this->options['thumbnail_field'],
      '#description' => t('This field should be a small square image.'),
      '#required' => 1,
    );

    //Select which field to use for captions
    $form['caption_field'] = array(
      '#type' => 'select',
      '#title' => t('caption field'),
      '#options' => $fields,
      '#default_value' => $this->options['caption_field'],
      '#description' => t('This unrequired field would most likely be assigned to a node body, a CCK text field, the description of an imagefield.'),
      '#required' => 0,
    );

    //Use AJAX or not
    $form['ajax'] = array(
      '#type' => 'select',
      '#title' => t('Use AJAX for "Show More" links'),
      '#options' => array('0' => 'No', '1' => 'Yes'),
      '#default_value' => $this->options['ajax'],
      '#description' => t('Choosing "Yes" will replace the normal pager for this View with a "Show More" link. When clicked, the link will bring back the next set of results using AJAX. This link will only appear when the total number of results for the View exceeds the "Items per page" limit. You must also have either the full or mini pager enabled.'),
      '#required' => 1,
    );

  }
}
