<?php
/**
 * @file
 * Define Views plugin using the Views API for image galleries optimized for mobile devices
 */

/**
 * Implementation of hook_views_plugins().
 */
function mobilegallery_semanticviews_views_plugins() {
  return array(
    'style' => array(
      'mobilegallery' => array(
        'title'           => t('Mobile Gallery: Semantic Views'),
        'help'            => t('Display the results using mobilegallery'),
        'handler'         => 'mobilegallery_semanticviews_plugin_style_mobilegallery_semanticviews',
        'uses options'    => TRUE,
        'uses row plugin' => TRUE,
        'uses fields'     => TRUE,
        'uses grouping'   => FALSE,
        'type'            => 'normal',
        'parent'          => 'semanticviews_default',
        //Use the semanticviews template to keep this module as light as possible
        'theme' => 'semanticviews_view_unformatted',
        'theme file' => 'semanticviews.theme.inc',
        'theme path' => drupal_get_path('module', 'semanticviews'),
      ),
    ),
  );
}
