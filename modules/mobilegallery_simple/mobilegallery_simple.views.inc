<?php
/**
 * @file
 * Define Views plugin using the Views API for image galleries optimized for mobile devices
 */

/**
 * Implementation of hook_views_plugins().
 */
function mobilegallery_simple_views_plugins() {
  return array(
    'style' => array(
      'mobilegallery_simple' => array(
        'title'           => t('Mobile Gallery: Simple'),
        'help'            => t('Display the results using mobilegallery'),
        'handler'         => 'mobilegallery_simple_plugin_style_mobilegallery_simple',
        'uses options'    => TRUE,
        'uses row plugin' => TRUE,
        'uses fields'     => TRUE,
        'uses grouping'   => FALSE,
        'type'            => 'normal',
        'parent'          => 'list',
        //Use the semanticviews template to keep this module as light as possible
        'theme' => 'mobilegallery_simple_rows',
        'theme file' => 'mobilegallery_simple.theme.inc',
        'theme path' => drupal_get_path('module', 'mobilegallery_simple'),
      ),
    ),
  );
}
