<?php foreach ($rows as $id => $row): ?>
  <div <?php print drupal_attributes($row_attributes[$id]); ?>>
    <?php print $row; ?>
  </div>
<?php endforeach; ?>
