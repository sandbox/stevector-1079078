<?php

/**
 * @file
 * Theme function for mobilegallery_simple
 */

/**
 * Display the simple view of rows one after another
 */
function template_preprocess_mobilegallery_simple_rows(&$vars) {

    $view = $vars['view'];

    $image_field = $view->style_plugin->options['image_field'];
    $imagecache_full_preset = $view->style_plugin->options['imagecache_full_preset'];

    //create the main image for each row
    foreach ($vars['rows'] as $key => $value) {
      $image_file = $view->field[$image_field]->theme($view->result[$key]);
      $vars['rows'][$key] = theme('imagecache', $imagecache_full_preset, $image_file, $alt = '', $title = '', $attributes = NULL, $getsize = FALSE);
    }

    $vars = theme('mobilegallery_prepro_views_rows', $view, $vars);
}

/**
 * Theme the thumbnails for the Mobile Gallery
 *
 * This function is called from mobilegallery_preprocess_views_view
 *
 * @param $view
 *   A Views object.
 * @return
 *   A string of images with links pointing to the ids of corresponding Mobile Gallery rows.
 */
function theme_mobilegallery_thumbnails__mobilegallery_simple($view) {

  $image_field = $view->style_plugin->options['image_field'];
  $imagecache_thumbnail_preset = $view->style_plugin->options['imagecache_thumbnail_preset'];

  // The number from which these rows will start counting up.
  $starting_index = $view->pager['current_page'] * $view->pager['items_per_page'];

  $output = '<span id="mg-top"></span>';
  foreach ($view->result as $key => $row) {
    $image_file = $view->field[$image_field]->theme($view->result[$key]);
    $thumbnail = theme('imagecache', $imagecache_thumbnail_preset, $image_file, $alt = '', $title = '', $attributes = NULL, $getsize = FALSE);

    $fragment = "mg-" . ($starting_index + $key);
    $attributes = array('class' => 'mobilegallery-asi thumbnails');
    $options = array(
      'attributes' => $attributes,
      'fragment' => $fragment,
      'html' => TRUE,
      // Necessary to get l() to do a simple anchor link
      'external' => TRUE,
    );
    $output .= l($thumbnail, '', $options);
  }

  return $output;
}
