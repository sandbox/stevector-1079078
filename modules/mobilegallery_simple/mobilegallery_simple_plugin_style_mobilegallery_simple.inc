<?php

/**
 * @file
 * Contains the mobilegallery simple style plugin.
 */

/**
 * Style plugin to provide a mobile image gallery
 *
 * @ingroup views_style_plugins
 */
class mobilegallery_simple_plugin_style_mobilegallery_simple  extends views_plugin_style_list {

  /**
   * Render the given style.
   */
  function options_form(&$form, &$form_state) {

    // Pre-build all of our option lists for the dials and switches that follow.
    $fields = array('' => t('<none>'));
    foreach ($this->display->handler->get_handlers('field') as $field => $handler) {
      if ($label = $handler->label()) {
        $fields[$field] = $label;
      }
      else {
        $fields[$field] = $handler->ui_name();
      }
    }


    $imagecache_presets  = imagecache_presets();
    $imagecache_preset_names = array();

    foreach ($imagecache_presets as $value) {
      $imagecache_preset_names[$value['presetname']] = $value['presetname'];
    }


    //Select which field is the image
    $form['image_field'] = array(
      '#type' => 'select',
      '#title' => t('image field'),
      '#options' => $fields,
      '#default_value' => $this->options['image_field'],
      '#description' => t('This field should yield the Drupal path to an image file.'),
      '#required' => 1,
    );

    //Select the imagecache preset for thumbnails
    $form['imagecache_thumbnail_preset'] = array(
      '#type' => 'select',
      '#title' => t('imagecache thumbnail preset'),
      '#options' => $imagecache_preset_names,
      '#default_value' => $this->options['imagecache_thumbnail_preset'],
      '#description' => t('The selected imagecache preset should be a small square image. If you are unsure of what size to use, "scale and crop" to 75px x 75px is recommended.'),
      '#required' => 1,
    );

    //Select the imagecache preset for full images
    $form['imagecache_full_preset'] = array(
      '#type' => 'select',
      '#title' => t('imagecache full preset'),
      '#options' => $imagecache_preset_names,
      '#default_value' => $this->options['imagecache_full_preset'],
      '#description' => t('The selected imagecache preset should be optimized for mobile devices. If you are unsure of what size to use, scaling to a maximum height or width of 480px is recommended.'),
      '#required' => 1,
    );

    //Select which field to use for the caption
    $form['caption_field'] = array(
      '#type' => 'select',
      '#title' => t('caption field'),
      '#options' => $fields,
      '#default_value' => $this->options['caption_field'],
      '#description' => t('This unrequired field would most likely be assigned to a node body, a CCK text field or the description of an imagefield.'),
      '#required' => 0,
    );

    //Use AJAX or not
    $form['ajax'] = array(
      '#type' => 'select',
      '#title' => t('Use AJAX for "Show More" links'),
      '#options' => array('0' => 'No', '1' => 'Yes'),
      '#default_value' => $this->options['ajax'],
      '#description' => t('Choosing "Yes" will replace the normal pager for this View with a "Show More" link. When clicked, the link will bring back the next set of results using AJAX. This link will only appear when the total number of results for the View exceeds the "Items per page" limit. You must also have either the full or mini pager enabled.'),
      '#required' => 1,
    );

  }
}
