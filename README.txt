Mobile Gallery
--------------

The Views Mobile Gallery module is a Views style plugin that outputs images
organized in a way that is optimized for mobile devices. For best results,
use this module with a mobile-optimized theme. Mobile Tools is not required
to for module but is a great way to switch between desktop and mobile themes.

There are currently 2 modes:

1. Simple

In the simple version of Mobile Gallery, you merely need to select an a field
for an image file and two imagecache presets for thumbnails and larger images.
Captions are optional.

2. Semantic Views

In the Semantic Views version of Mobile Gallery you select a field for the
thumbnail image. In this case the thumbnail field should be a rendered <img tag.
This is provided out of the box by imagefield. You also have the option to
selected a caption field. Any remaining fields will appear in "Single Image Mode"
when a thumbnail is clicked.

It's recommended that if you are using this module for the first time, that you
try the simple version to get a sense of how it works even if you will eventually need
the Semantic views version.


Degradation
----------

This module will bring both the thumbnail and full versions of images at page load.
Javascript is then used to show and hide the larger images when thumbnails and
navigation links are clicked/touched. Because each thumbnail is wrapped with a basic
anchor tag, native browser behavior should function without Javascript and cause the
window to simply jump to the full version of the given image.


Mobile Device Orientation
-------------------------

This module uses CSS3 to attempt to detect the orientation of the device being used
to view the page. When single images are viewed in portrait orientation, the width
of the image is set to 100% so as not to exceed the boundaries of the device. When
the device rotates to landscape, the height is set to 100%.


CSS in your theme
-----------------

When a thumbnail is clicked/touched the intended behavior is for the screen to go
black and the user will see the larger version of the selected image along with
navigation buttons and possibly a caption. Depending on your theme, you may need
to adjust margins, padding or other CSS properties for optimal results.


Overriding theme functions
--------------------------

The calls to theme functions in this module borrow concepts and code from
the main Views module to allow for very granular overrides. For example, before
going to the main implementation of the function 'mobilegallery_close,' Drupal's
theming system would first check to see if more functions exist pertaining
to the specific View being dealt with. And before that, the theming system
would even check to see if the given submodule being used, mobilegallery_simple or
mobilegallery_semanticviews has an override.

So instead of just passing 'mobilegallery_close' as a string to theme(),
an array of potential functions is sent instead.

For example, if the hook being passed is 'mobilegallery_close' and the submodule
being used for the style plugin is 'mobilegallery_simple' and the view name
is 'imagecenter' and the variant is 'panel_pane_1', the array would be:
array(
   'mobilegallery_close__mobilegallery_simple__imagecenter__panel_pane_1',
   'mobilegallery_close__mobilegallery_simple__panel_pane_1',
   'mobilegallery_close__mobilegallery_simple__',
   'mobilegallery_close__mobilegallery_simple__imagecenter__panel_pane',
   'mobilegallery_close__mobilegallery_simple__panel_pane',
   'mobilegallery_close__mobilegallery_simple__imagecenter',
   'mobilegallery_close__mobilegallery_simple',
   'mobilegallery_close__imagecenter__panel_pane_1',
   'mobilegallery_close__panel_pane_1',
   'mobilegallery_close__',
   'mobilegallery_close__imagecenter__panel_pane',
   'mobilegallery_close__panel_pane',
   'mobilegallery_close__imagecenter',
   'mobilegallery_close',
 );

This approach allows submodules like mobilegallery_simple and
mobilegallery_semanticviews to define their own overrides and for the end
themer to have tight control over overrides as well.


Author
-------

Steve Persch (stevector)
http://drupal.org/user/179805
