/**
 * @file
 * The CSS for Mobile Gallery
 */

Drupal.behaviors.MobileGallery = function(context) {

  //Add a body class for easier theming and to identify
  //that javascript has loaded
  $('body').addClass('mobilegallery');

  /** Remove the width and height attributes put in
   *  by imagecache.
   *
   *  This is necessary to to set widths and heights
   *  to 100% via CSS depending on the orientation of
   *  The mobile device.
   *
   *  During initial module development, I accomplished this
   *  with a theme-layer override of theme_imagecache.
   *  Perhaps this module could accomplish the same thing
   *  with hook_theme_registry_alter or a just another field formatter
   */
  $('.mobilegallery-rows img').attr('width', '');
  $('.mobilegallery-rows img').attr('height', '');


  //Remove and add back first and last class
  //This is necessary because with each new AJAX request
  //More rows come back with what they think they have a new 'first' and 'last'
  //This JS will identify the real first and last
  $('.mobilegallery-rows.last').removeClass('last');
  $('.mobilegallery-rows.first').removeClass('first');
  $('.mobilegallery-rows:last').addClass('last');
  $('.mobilegallery-rows:first').addClass('first');

  //Identify all the parents of the gallery for CSS showing and hiding
  $('.mobilegallery-rows.first').parents().addClass('mobilegallery-parent');

  //Adds DOM selections as jQuery objects to the Drupal object.
  mobilegallery_jquery_optimize();

  //Preload navigation images.
  //This only needs to run at page load
  //Not when more images come over JSON
  mobilegallery_preload_images();

  //A link to an image. When clicked, active the single-image mode
  //asi = "active single-image"
  Drupal.settings.mobilegallery.opt.mobilegallery_asi.click(function() {
    Drupal.settings.mobilegallery.opt.body.addClass('mobilegallery-single-image');
    var hash_id = $(this).attr('href');
    Drupal.settings.mobilegallery.opt.mobilegallery_rows.removeClass('mobilegallery-active-image');
    $(hash_id).addClass('mobilegallery-active-image');
    return false;
  });

  //The X icon. Get out of single-image mode when clicked
  Drupal.settings.mobilegallery.opt.mobilegallery_close.click(function() {
    Drupal.settings.mobilegallery.opt.body.removeClass('mobilegallery-single-image');
    return false;
  });

  //When the showmore link is clicked, grab it and use it for JSON.
  Drupal.settings.mobilegallery.opt.mobilegallery_showmore_link.click(function() {
    //Add a body class when we're loading
    Drupal.settings.mobilegallery.opt.body.addClass('mobilegallery-loading');

      //Grab the URL to which this link was pointing
      var url = $(this).attr('href');
      mobilegallery_showmore(url);

    return false;
  });
}

/**
 * Adds DOM selections as jQuery objects to the Drupal object for better performance
 */
function mobilegallery_jquery_optimize() {
  Drupal.settings.mobilegallery.opt = new Object;
  Drupal.settings.mobilegallery.opt.body                             = $('body');
  Drupal.settings.mobilegallery.opt.mobilegallery_asi                = $("a.mobilegallery-asi");
  Drupal.settings.mobilegallery.opt.mobilegallery_close              = $("a.mobilegallery-controls.mobilegallery-close");
  Drupal.settings.mobilegallery.opt.mobilegallery_showmore_link      = $("a.mobilegallery-showmore-link");
  Drupal.settings.mobilegallery.opt.mobilegallery_rows               = $('.mobilegallery-rows');
  Drupal.settings.mobilegallery.opt.mobilegallery_rows_wrapper       = $('.mobilegallery-rows-wrapper');
  Drupal.settings.mobilegallery.opt.mobilegallery_thumbnails_wrapper = $('.mobilegallery-thumbnails-wrapper');
  Drupal.settings.mobilegallery.opt.mobilegallery_showmore_wrapper   = $('.mobilegallery-showmore-wrapper');
}

/**
 * Get the next page over JSON when the show more link is clicked
 */
function mobilegallery_showmore(url) {

  //add another GET variable to indicate to PHP that JSON should be returned
  var json_url = url + '&mobilegallery_json=1';

  //Get the JSON and do some replacing, appending
  $.getJSON(json_url, function(json) {

    //completely replace the showmore link
    Drupal.settings.mobilegallery.opt.mobilegallery_showmore_wrapper.html(json.showmore_link);

    //tack on the thumbs and rows
    Drupal.settings.mobilegallery.opt.mobilegallery_thumbnails_wrapper.append(json.thumbnails);
    Drupal.settings.mobilegallery.opt.mobilegallery_rows_wrapper.append(json.rows);

    //Attach behaviors again to get all this code to run again
    Drupal.attachBehaviors();

    //Remove this loading class to get rid of the loading gif
    Drupal.settings.mobilegallery.opt.body.removeClass('mobilegallery-loading');
  });
}

/**
 * preload navigation images
 */
function mobilegallery_preload_images() {

  for(key in Drupal.settings.mobilegallery.images) {
    //Make new object to hold Images
    Drupal.settings.mobilegallery.loaded_images = new Object;
    //Make a new image
    Drupal.settings.mobilegallery.loaded_images[key] = new Image();
    //Set the source to be the path of the needed image
    Drupal.settings.mobilegallery.loaded_images[key].src = Drupal.settings.mobilegallery.images[key];
  }
}
