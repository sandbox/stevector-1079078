<?php
/**
 * @file
 * Theme function for mobilegallery
 */

/**
 * Add variables to the mobilegallery_gallery template
 */
function template_preprocess_mobilegallery_gallery($vars) {
  $view = $vars['view'];
  $vars['close_link'] = mobilegallery_render('mobilegallery_close', $view);
}

/**
 * Theme the thumbnails for the Mobile Gallery
 *
 * This function is called from mobilegallery_preprocess_views_view
 *
 * @param $view
 *   A Views object.
 * @return
 *   A string of images with links pointing to the ids of corresponding Mobile Gallery rows.
 */
function theme_mobilegallery_thumbnails($view) {

  //The machine name of the field used for thumbnails.
  $thumbnail_field =  $view->style_plugin->options['thumbnail_field'];

  //The number from which these rows will start counting up.
  $starting_index = $view->pager['current_page'] * $view->pager['items_per_page'];

  $output = '<span id="mg-top"></span>';
  foreach ($view->result as $num => $row) {
    $thumbnail = $view->field[$thumbnail_field]->theme($row);

    $fragment = "mg-" . ($starting_index + $num);
    $attributes = array('class' => 'mobilegallery-asi thumbnails');
    $options = array(
      'attributes' => $attributes,
      'fragment' => $fragment,
      'html' => TRUE,
      // Necessary to get l() to do a simple anchor link.
      'external' => TRUE,
    );
    $output .= l($thumbnail, '', $options);
  }

  return $output;
}

/**
 * Creates a link to close single-image mode.
 *
 * This function is called from template_preprocess_mobilegallery_gallery
 *
 * @param $view
 *   A Views object. It is currently not being used by this function.
 *   It may be necessary in an override.
 * @return
 *   An 'a' tag with classes that correspond to JS functions
 */
function theme_mobilegallery_close($view) {

  $text = '<span>' . t('Back to top') . '</span>';
  $fragment = 'mg-top';
  $attributes = array('class' => 'mobilegallery-asi mobilegallery-controls mobilegallery-close');
  // return l($text, '', array('attributes' => $attributes, 'fragment' => $fragment, 'html' => TRUE));

  $options = array(
    'attributes' => $attributes,
    'fragment' => $fragment,
    'html' => TRUE,
    // Necessary to get l() to do a simple anchor link.
    'external' => TRUE,
  );
  $output = l($text, '', $options);

  return $output;
}

/**
 * Creates the caption for a given Views result
 *
 * This function is called from mobilegallery_preprocess_semanticviews_view_unformatted
 *
 * @param $view
 *   A Views object.
 * @param $caption_field
 *   A string that indicates, by machine readable name, the field in
 *   the results that is the caption
 * @param $key
 *   The array key of the result for which the caption is being produced.
 * @return
 *   The themed caption field for this row wrapped in a div
 */
function theme_mobilegallery_caption($view, $caption_field, $key = 0) {

  $text = $view->field[$caption_field]->theme($view->result[$key]);

  if (!empty($text)) {
    $output = "<div class='mobilegallery-caption'>
      ". $text . "
    </div>";
  }
  else {
    $output = '';
  }

  return $output;
}

/**
 * Creates the next and previous links for a given row.
 *
 * This function is called from mobilegallery_preprocess_semanticviews_view_unformatted
 *
 * @param $view
 *   A Views object. It is currently not being used by this function.
 *   It may be necessary in an override.

 * @param $starting_index
 *   The result number on which this page request start in relation to the total number of row.
 *   For example, a View may have the number of items per page set to 12. The starting index for the
 *   fourth page of results would be 36.
 * @param $key
 *   The result number for this row in relation to the number of rows in just this query.
 * @return
 *   A string of next and previous link for a given row wrapped in a div.
 */
function theme_mobilegallery_navigation($view, $starting_index = 0, $key = 0) {

  // Make the Next link.
  $text = '<span>' . t('Next') . '</span>';
  $fragment = 'mg-' . ($starting_index + $key + 1);
  $attributes = array('class' => 'mobilegallery-asi mobilegallery-controls mobilegallery-next-link');
  $options = array(
    'attributes' => $attributes,
    'fragment' => $fragment,
    'html' => TRUE,
    // Necessary to get l() to do a simple anchor link.
    'external' => TRUE,
  );
  $next = l($text, '', $options);

  // Make the Previous link
  $text = '<span>' . t('Previous') . '</span>';
  $fragment = 'mg-' .  ($starting_index + $key - 1);
  // Most of the options can be re-used, just reassign classes and the fragment.
  $options['attributes']['class'] = 'mobilegallery-asi mobilegallery-controls mobilegallery-prev-link';
  $options['fragment'] = $fragment;

  $prev = l($text, '', $options);

  $output = "<div class='mobilegallery-nav'>
    $prev
    $next
  </div>";

  return $output;
}

/**
 * Creates the showmore link
 *
 *
 * This function is called from mobilegallery_preprocess_views_view
 *
 * This only gets called when the style plugin is configured to use it
 * and the view is configured to use a pager.
 *
 * @param $view
 *   A Views object.
 * @return
 *   A link to the next page of results. Javascript will stop the normal
 *   behavior of this link and grab and insert JSON instead.
 */
function theme_mobilegallery_showmore_link($view) {

  $last_record_in_this_request = count($view->result) + ($view->pager['items_per_page'] * $view->pager['current_page']);

  //Use the numeric variables in this text string
  //that will yield something "Showing 18 of 41"
  $count_text = t("Showing @currently_show of @total_rows_in_view", array('@currently_show' => $last_record_in_this_request, '@total_rows_in_view' => $view->total_rows));

  //Create a show more link if we're not on the last page
  if ($last_record_in_this_request < $view->total_rows) {
    $link_array = array(
      //the next page
      'query' => array('page' => ($view->pager['current_page'] + 1)),
      'attributes' => array('class' => 'mobilegallery-showmore-link')
    );
    $next_link = l(t('Show More'), $_GET['q'], $link_array) . ' - ';
  }

  return $next_link . $count_text;
}

/**
 * Adds JS and CSS files and settings JSON objects to the page
 *
 * This is done with a theme function so that if it can be easily
 * Override in a submodule or template.php file. This function adds
 * Images filepaths to the settings within the Drupal JS object.
 * Those images are then preloaded by the browser with javascript.
 * This is necessary because the images are background images for
 * UI elements in the next/prev buttons. If the browser just loaded
 * them normally there would be extra lag time, especially on a wireless
 * network. It is likely that these background images will be overridden
 * In the theme layer to change colors and such.
 *
 * @param $view
 *   A Views object.
 * @return
 *   Although this is technically a theme function, nothing is returned.
 */
function theme_mobilegallery_add_js_and_css($view) {

  //Get the path of this module
  $mod_path = drupal_get_path('module', 'mobilegallery');

  $js_settings = array(
    'mobilegallery' => array(
      'images' => array(),
    ),
  );

  //All the navigation images. These will get preloaded with JS
  $images = array(
    'close-icon.png',
    'left-arrow.png',
    'loading-thumbails.png',
    'loading.gif',
    'right-arrow.png',
  );

  //Get the URL for each image
  foreach ($images as $image) {
    $js_settings['mobilegallery']['images'][] = url($mod_path . '/images/' .  $image);
  }

  //Add the images to the settings JSON object
  drupal_add_js($js_settings, 'setting');

  //Add the CSS and JS sheets
  drupal_add_css($mod_path . '/mobilegallery.css');
  drupal_add_js($mod_path . '/mobilegallery.js');
}
